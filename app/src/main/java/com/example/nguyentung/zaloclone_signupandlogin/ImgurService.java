package com.example.nguyentung.zaloclone_signupandlogin;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by thanhtung on 24/06/2017.
 */

interface ImgurService {

    @retrofit2.http.Multipart
    @retrofit2.http.Headers({
            "Authorization: Client-ID 0b5e46b0ac7b39f"
    })
    @retrofit2.http.POST("image")
    Call<ImageResponse> postImage(
//            @retrofit2.http.Query("title") String title,
//            @retrofit2.http.Query("description") String description,
            @retrofit2.http.Query("album") String albumId,
            @retrofit2.http.Query("thanhtungth94") String username,
            @retrofit2.http.Part MultipartBody.Part file);


    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.imgur.com/3/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
