package com.example.nguyentung.zaloclone_signupandlogin;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hbb20.CountryCodePicker;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class SignupActivity extends AppCompatActivity {

    CircleImageView imgTakePhotoSignup;
    EditText edtEmailSignup;
    EditText edtNicknameSignup;
    EditText edtPasswordSignup;
    TextView txtMaleSignup;
    TextView txtFemaleSignup;
    TextView txtBirthdaySignup;
    CountryCodePicker ccpSignup;
    EditText edtPhonenumberSignup;
    ImageView imgBackSignup;
    TextView txtFinishSignup;
    String gender;
    String latitude;
    String longitude;

    private static final int CAMERA_REQUEST = 1888;
    private static final int PICK_IMAGE = 999;
    Uri uri = null;
    DatabaseReference root = FirebaseDatabase.getInstance().getReference().child("ChatApp").child("Users");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        imgTakePhotoSignup = (CircleImageView) findViewById(R.id.imgTakePhotoSignup);
        edtEmailSignup = (EditText) findViewById(R.id.edtEmailSignup);
        edtNicknameSignup = (EditText) findViewById(R.id.edtNicknameSignup);
        edtPasswordSignup = (EditText) findViewById(R.id.edtPasswordSignup);
        txtMaleSignup = (TextView) findViewById(R.id.txtMaleSignup);
        txtFemaleSignup = (TextView) findViewById(R.id.txtFemaleSignup);
        txtBirthdaySignup = (TextView) findViewById(R.id.txtBirthdaySignup);
        ccpSignup = (CountryCodePicker) findViewById(R.id.ccpSignup);
        edtPhonenumberSignup = (EditText) findViewById(R.id.edtPhonenumberSignup);

        Toolbar tb = (Toolbar) findViewById(R.id.actionbar_play);
        setSupportActionBar(tb);

        imgBackSignup = (ImageView) findViewById(R.id.imgBackSignUp);
        txtFinishSignup = (TextView) findViewById(R.id.txtFinishSignup);

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY", Locale.US);
                txtBirthdaySignup.setText(sdf.format(myCalendar.getTime()));
            }
        }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));

        txtBirthdaySignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });

        txtMaleSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtMaleSignup.setBackgroundResource(R.drawable.item_selected_gender);
                txtMaleSignup.setTextColor(Color.WHITE);
                txtFemaleSignup.setBackgroundResource(R.drawable.item_gender_textview);
                txtFemaleSignup.setTextColor(Color.parseColor("#80C8A7"));
                gender = "Male";
            }
        });

        txtFemaleSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtFemaleSignup.setBackgroundResource(R.drawable.item_selected_gender);
                txtFemaleSignup.setTextColor(Color.WHITE);
                txtMaleSignup.setBackgroundResource(R.drawable.item_gender_textview);
                txtMaleSignup.setTextColor(Color.parseColor("#80C8A7"));
                gender = "Female";
            }
        });

        imgTakePhotoSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(SignupActivity.this);
                dialog.setTitle("Choose avatar");
                dialog.setPositiveButton("Take photo", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(i, CAMERA_REQUEST);
                    }
                });
                dialog.setNegativeButton("Open gallery", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        photoPickerIntent.setType("image/*");
                        startActivityForResult(Intent.createChooser(photoPickerIntent, "Select image from Gallery"), PICK_IMAGE);
                    }
                });
                dialog.show();
            }
        });

        imgBackSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        AddUser();

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            imgTakePhotoSignup.setImageBitmap(photo);
        }
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            InputStream imageStream = null;
            try {
                imageStream = getContentResolver().openInputStream(selectedImage);
                Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                imgTakePhotoSignup.setImageBitmap(yourSelectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
        }

        imgBackSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SignupActivity.this, "aaaa", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void AddUser() {
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // ...
                            latitude = String.valueOf(location.getLatitude());
                            longitude = String.valueOf(location.getLongitude());
                            txtFinishSignup.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        Map<String, Object> map = new HashMap<>();
                                        String temp_key = root.push().getKey();
                                        root.updateChildren(map);

                                        DatabaseReference infoUser = root.child(temp_key);
                                        Map<String, Object> map2 = new HashMap<>();
                                        map2.put("DeviceType", Build.MODEL);
                                        infoUser.updateChildren(map2);
                                        map2 = new HashMap<>();
                                        map2.put("birthday", txtBirthdaySignup.getText());
                                        infoUser.updateChildren(map2);
                                        map2 = new HashMap<>();
                                        map2.put("bundle_id", getApplicationContext().getPackageName());
                                        infoUser.updateChildren(map2);
                                        map2 = new HashMap<>();
                                        map2.put("calling_code", "(+" + ccpSignup.getSelectedCountryCode() + ")");
                                        infoUser.updateChildren(map2);
                                        TelephonyManager telephonyManager = ((TelephonyManager) SignupActivity.this.getSystemService(Context.TELEPHONY_SERVICE));
                                        String carrier = telephonyManager.getNetworkOperatorName();
                                        map2 = new HashMap<>();
                                        map2.put("carrier", carrier);
                                        infoUser.updateChildren(map2);
                                        map2 = new HashMap<>();
                                        map2.put("country_code", ccpSignup.getSelectedCountryName());
                                        map2 = new HashMap<>();
                                        infoUser.updateChildren(map2);
                                        map2 = new HashMap<>();
                                        map2.put("device_name", Build.DEVICE);
                                        infoUser.updateChildren(map2);
                                        String token = FirebaseInstanceId.getInstance().getToken();
                                        map2 = new HashMap<>();
                                        map2.put("device_token", token);
                                        infoUser.updateChildren(map2);
                                        map2 = new HashMap<>();
                                        map2.put("email", edtEmailSignup.getText().toString());
                                        infoUser.updateChildren(map2);
                                        map2 = new HashMap<>();
                                        map2.put("is_verify", 0);
                                        infoUser.updateChildren(map2);
                                        map2 = new HashMap<>();
                                        map2.put("last_active", "");
                                        infoUser.updateChildren(map2);
                                        //=========================Put current location========================================================
                                        map2 = new HashMap<>();
                                        map2.put("latitude", latitude);
                                        infoUser.updateChildren(map2);
                                        map2 = new HashMap<>();
                                        map2.put("longitude", longitude);
                                        infoUser.updateChildren(map2);
//                    //============================================================================================
                                        map2 = new HashMap<>();
                                        map2.put("nickname", edtNicknameSignup.getText().toString());
                                        infoUser.updateChildren(map2);
                                        map2 = new HashMap<>();
                                        map2.put("password", edtPasswordSignup.getText().toString());
                                        infoUser.updateChildren(map2);
                                        map2 = new HashMap<>();
                                        map2.put("phone_number", edtPhonenumberSignup.getText().toString());
                                        infoUser.updateChildren(map2);
                                        map2 = new HashMap<>();
                                        map2.put("sex", gender);
                                        infoUser.updateChildren(map2);
                                        //get wifi name
                                        @SuppressLint("WifiManagerLeak") WifiManager wifiMgr = (WifiManager) SignupActivity.this.getSystemService(Context.WIFI_SERVICE);
                                        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
                                        String name = wifiInfo.getSSID();
                                        name = name.substring(1, name.length() - 1);
                                        map2 = new HashMap<>();
                                        map2.put("wifi_name", name);
                                        //===================================
                                        Toast.makeText(SignupActivity.this, name, Toast.LENGTH_SHORT).show();
                                        infoUser.updateChildren(map2);
                                        //      Toast.makeText(SignupActivity.this, root.push().getKey(), Toast.LENGTH_SHORT).show();
                                        //        AddUser();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                            });
                        }
                    }
                });
    }
}
