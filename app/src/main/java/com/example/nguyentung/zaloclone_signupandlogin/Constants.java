package com.example.nguyentung.zaloclone_signupandlogin;

/**
 * Created by thanhtung on 24/06/2017.
 */

public interface Constants {
    public static final int PICK_IMAGE_REQUEST = 1;
    public static final int READ_EXTERNAL = 1001;
    public static final int WRITE_EXTERNAL = 1002;
    public static final int READ_WRITE_EXTERNAL = 1003;
}
